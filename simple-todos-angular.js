Posts = new Mongo.Collection('posts');
 // This code only runs on the client
  if (Meteor.isClient) {
 
  Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
  });

  angular.module('simple-todos',['angular-meteor','accounts.ui']);
 
  function onReady() {
    angular.bootstrap(document, ['simple-todos']);
  }
  if (Meteor.isCordova)
    angular.element(document).on('deviceready', onReady);
  else
    angular.element(document).ready(onReady);
 
  angular.module('simple-todos').controller('TodosListCtrl', ['$scope', '$meteor',
    function ($scope, $meteor) {
        $scope.posts = $meteor.collection( function() {
        return Posts.find({}, { sort: { createdAt: -1 } })
      });
      /*$scope.posts = [

        { text: "This is post 1",
          postedBy: "Raj Vaibhav Singh",
          postType: "posted in his own timeline",
          postDate: "2 days ago" 
        },
        { text: 'This is post 2',
          postedBy: "Ankit Gaurav",
          postType: "posted in his own timeline",
          postDate: "3 days ago"
        },
        { text: 'This is post 3' ,
          postedBy: "Himank Dixit",
          postType: "posted in his own timeline",
          postDate: "4 days ago"
        }
      ];*/
       $scope.deletePost = function (post) {
      if (post.postedBy===Meteor.user().username) 
        {
          id=post._id;
          Posts.remove(id);
        } 
        else
          {
            alert("You cannot delete someone else's post");
          };
        }
     
      $scope.addPost = function (newPost) {
        $scope.posts.push( {
          text: newPost,
          owner: Meteor.userId(),            // _id of logged in user
          postedBy: Meteor.user().username,
          postType:"posted on his page",   // username of logged in user
          createdAt: new Date() }
        );
      };
 
  }]);
}