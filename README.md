_This is the first official documentation of Readaholic_
> Rate the web !

Why Readaholic
--------------

+ Post a question and get answers from people around using gps
+ Your intellect  finds acceptance from world
+ Become a super blogger without learning to optimize your website
+ Make khan academy social
+ Show off your knowledge instead of Photo
+ Build your circle of friends and followers to discuss specific issues
+ now wake up everyday to learn things instead of just watching selfies or memes
+ blogs , posts  , status, tutorials , website contents  contents


Key Terminoloy
--------------

1. Readaholic
2. Reads
3. Circles
4. Likes
5. Topics

css rules
--------------
* instead of camel case use '-' for multi word classes

Javascipt rules
-------------------
+ Use camelCase for variable names
+ Capitalised Classname
+ no explicit "class" keyword, function to define a class
+ class function doubles as constructor for objects
+ use === instead of == and !== instead of != for equality checking
+ avoid the use of "with"
+ Start private variables with an underscore "_"
+ We’ll be referring to Meteor Methods with a capital M to differentiate them from class        methods in JavaScript.
+ avoid using eval


Dependencies
------------
+ semantic-ui
+ iron-router
+ [Pretty email](https://atmospherejs.com/yogiben/pretty-email)
+ [Fake data](https://atmospherejs.com/digilord/fakerL)
+ [aldeen:simple schema](https://atmospherejs.com/aldeed/simple-schema)
+ [aldeen:collection schema2](https://atmospherejs.com/aldeed/collection2)
+ [meteor toys](https://meteor.toys/)



How to run tests
---------------- 


Deployment instructions
-----------------------
+ [http://readaholic_app.meteor.com/](http://readaholic_app.meteor.com/)
+ [http://readaholic1.meteor.com/](http://readaholic1.meteor.com/)


Future reads
------------
+ [google webmaster tool](https://www.google.com/webmasters/tools/home)
+ [google javascript crawler](https://googlewebmastercentral.blogspot.in/2014/05/understanding-web-pages-better.html)
+ [Push state url](http://www.analog-ni.co/precomposing-a-spa-may-become-the-holy-grail-to-seo)
   --a.k.a. holy grail for seo of spa
+ [read this answer for seo](http://stackoverflow.com/questions/13499040/how-do-search-engines-deal-with-angularjs-applications)