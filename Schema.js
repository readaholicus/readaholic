Schemas={};
	Schemas.userCustom = new SimpleSchema({
	  name: {
	  	type: String
	  },
	  userId: {
	  	type: String,
	  	regEx: SimpleSchema.RegEx.Id,
	  },
	  password:{
	  		type: String
	  },
	  followers:{
	  				type:[String],
	  				optional:true
	  },
	  following:{
	  				type:[String],
	  				optional:true
	  },
	  posts:{
	  				type:[String],//Contains Post id
	  				optional:true
	  },
	  groups:{
	  				type:[String],
	  				optional:true
	  },
	  comments:{
	  				type:[String],
	  				optional:true
	  },
	  likes:{
	  				type:[String],
	  				optional:true
	  },
	  readings:{
	  				type:[String],
	  				optional:true
	  },
	  url:{
	  	type:String,
	  	optional:true
	  }
	});
	Schemas.userComments = new SimpleSchema({
	 postId:{
	 	type:String
	 },
	 userId:{
	 	type:String
	 },
	 text:{
	 	type:String
	 },
	 commenOn:{
	 	type:String,
	 	optional:true
	 }
	});