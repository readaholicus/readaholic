Posts = new Mongo.Collection("tasks");
UserCustom=new Mongo.Collection("userCustom");
UserCustom.attachSchema(Schemas.userCustom);
UserComments=new Mongo.Collection("userComments");
UserComments.attachSchema(Schemas.userComments);
if (Meteor.isClient) {
  // This code only runs on the client
    Meteor.subscribe("tasks");
    Meteor.subscribe("userCustom");
    Meteor.subscribe("userComments");
    Session.set("followText","Follow");
    Session.set("followColor","blue");
   Template.register.helpers({
    userObjects: function(){
      return UserCustom.find({});
    },
    posts: function () {
      var route = Router.current();
       var urlName=route.params.prettyName;
       var visitedPageUser=findUserId(urlName);
     return Posts.find({owner:visitedPageUser}, {sort: {createdAt: -1}});
    },
    userId:function(){
      return Meteor.userId();
    },
    numberOfFollowers:function(){
      var route = Router.current();
       var urlName=route.params.prettyName;
       var visitedPageUser=findUserId(urlName);
      return UserCustom.findOne({userId:visitedPageUser}).followers.length;
    },
    follow:function(){
      var route = Router.current();
       var urlName=route.params.prettyName;
       var visitedPageUser=findUserId(urlName);
        var followArray=UserCustom.findOne({userId:visitedPageUser}).followers;
        var returnObject={};
        returnObject.number=followArray.length;
        
        if (Meteor.user() && followArray.contains(Meteor.userId())) {
          returnObject.text="Following";
          returnObject.following=true;
        } else {
          returnObject.text="Follow";
          returnObject.following=false;
        }
        return returnObject;
    }
   });
   Template.register.events({
    "click .js-follow":function(event){
        if (Meteor.user()) 
          {
            var route = Router.current();
            var urlName=route.params.prettyName;
            var visitedPageUser=findUserId(urlName);
            Meteor.call("addFollower",visitedPageUser,Meteor.userId());

          }
          else 
          {
              alert("You need to log in now");
          }
    }
   });
  Template.home.helpers({
    posts: function () {
     return Posts.find({}, {sort: {createdAt: -1}});
    },
    userId:function(){
      return Meteor.userId();
    }
  });
  Template.home.events({
    "submit .new-post": function (event) {
      // Prevent default browser form submit
      event.preventDefault();
      // Get value from form element
      var text = event.target.text.value;

      if(text != ""){
        // Call the post method
        Meteor.call("newPost", text);
        // Clear form
        event.target.text.value = "";
      }
    },
    "click .delete": function () {
      Meteor.call("deletePost",this._id);
    }
  });
  Template.post.onCreated(function(){

  });
  Template.post.helpers({
    likedByUser: function(){
      if($.inArray(Meteor.userId(), this.like)!=-1)
      {
        return true;
      }
      else
      {
        return false;
      }
      
    }
  });
  Template.post.events({
    "click .js-like":function (){
      if (Meteor.user()) 
      {

        Meteor.call("addLike",this,Meteor.userId()); 
      }
      else 
      {
        alert("you must be logged in !!");
      }    
    },
    "click .js-show-comments":function (){
      if(event.target.parentElement.parentElement.parentElement.lastElementChild.lastElementChild.classList.contains("hidden")) 
      {
        event.target.parentElement.parentElement.parentElement.lastElementChild.lastElementChild.classList.remove("hidden");
      }
      else 
      {
        event.target.parentElement.parentElement.parentElement.lastElementChild.lastElementChild.classList.add("hidden");
      }
    },
    "submit .js-reply-form":function(){
      // Prevent default browser form submit
      event.preventDefault();
      Meteor.call("addComment",this._id,Meteor.userId(),event.target[0].value);
    }
  });
  Template.comment.helpers({
     findName:function(id){
      console.log("find name "+id);
       var name= UserCustom.findOne({userId:id}).name;
       return name;
    },
    findUrl:function(id){
      return findUserUrl(id);
    }
  });
  Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
  });
}
findUserUrl=function(id){
  var user=UserCustom.findOne({userId:id});
  return user.url;
}
findUserId=function(url){
  var user=UserCustom.findOne({url:url});
  return user.userId;
}
Array.prototype.contains = function ( needle ) {
   for (i in this) {
       if (this[i] == needle) return true;
   }
   return false;
}
/*Posts.allow({
  update: function (userId, doc) {
    return true;
  }
});*/