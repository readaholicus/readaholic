Meteor.publish("tasks", function () {
    return Posts.find();
  });
Meteor.publish("userCustom", function () {
    return UserCustom.find();
  });
Meteor.publish("userComments", function () {
    return UserComments.find();
  });
Meteor.methods({
  newPost: function (text) {
    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }
    var newPost={
          text: text,
          createdAt: new Date(), // current time text: text,           // current time
          owner: Meteor.userId(),           // _id of logged in user
          username: Meteor.user().username,
          like:[,

          ],
          likes:0,
          comments:[
          ]
        };
    Posts.insert(newPost,function(error,result){
      if (error) 
        {
          console.log("There was some error")
        } 
      else 
      {
        UserCustom.update({userId:Meteor.userId()},{$addToSet: {posts:result}});
      }
    });
  },
  deletePost: function (postId) {
          Posts.remove(postId);
  },
  addLike: function (post,userId) {
    if (Meteor.user()){
          if(!post.like.contains(userId)){
            Posts.update({_id:post._id},{$inc: { likes: 1 }},function(error,rows){
              if (error) {
                console.log("There was some error");
              } 
              else {
                Posts.update({_id:post._id},{$addToSet: {like:userId}},function(error,rows){
                  if (error) {
                    console.log("There was some error");
                  } 
                  else 
                  {
                    UserCustom.update({userId:Meteor.userId()},{$addToSet: {likes:post._id}});
                  }
                });
              }
            });
          }
          else
          {
            /*Posts.update({_id:post._id},{$pull: {like: userId}});
            Posts.update({_id:post._id},{$inc: { likes: -1 }});*/
            Posts.update({_id:post._id},{$inc: { likes: -1 }},function(error,rows){
              if (error) {
                console.log("There was some error");
              } 
              else {
                Posts.update({_id:post._id},{$pull: {like:userId}},function(error,rows){
                  if (error) {
                    console.log("There was some error");
                  } 
                  else 
                  {
                    UserCustom.update({userId:Meteor.userId()},{$pull: {likes:post._id}});
                  }
                });
              }
            });
          }
      } 
  },
  addComment: function(postId,userId,text){
     Posts.update({_id:postId},{$push: {comments:{by:userId,text:text} }},function(error,rows){
      if(error){
        console.log("there was some error in commenting");
      }
      else{
          var newComment={
            postId:postId,
             userId:userId,
             text:text
          };
          /*console.log(newComment);*/
          UserComments.insert(newComment,function(error,result){
            if (error) {
              console.log("There was some error in inserting comment in comment schema");
            } 
            else {
                UserCustom.update({userId:Meteor.userId()},{$addToSet: {comments:result}});
            }
          });
      }
     });
  },
  addFollower:function(to,follower){
    var followArray=UserCustom.findOne({userId:to}).followers;
    if (followArray.contains(follower)){
      UserCustom.update({userId:to},{$pull:{followers:follower}});
    } 
    else{
      UserCustom.update({userId:to},{$addToSet:{followers:follower}});
    }
    
  },
  addUser:function(name){
    User.insert({
      name:name,
      password:"letmein"
    });
  }
});
Array.prototype.contains = function ( needle ) {
   for (i in this) {
       if (this[i] == needle) return true;
   }
   return false;
}
// Support for playing D&D: Roll 3d6 for dexterity
Accounts.onCreateUser(function(options, user) {
 /* var url=user.username;
  url=url.*/
   UserCustom.insert({
    name:user.username,
    password:user.services.password.bcrypt,
    userId:user._id,
    followers:[],
    following:[],
    url:  user.username.replace(/ /g,"_")
   });
   return user;
});